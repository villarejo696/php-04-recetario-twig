<?php
include 'lib/funciones.php';

session_start();
/*if (!isset($_SESSION['user'])){
*	header("Location: login.php");
};*/
$receta_id = $_GET['receta_id'];
$template = $twig -> loadTemplate("recetas/receta.html");
$titulo = "Detalle Receta";
$resultado = detalle($receta_id);
$datos = array(
		titulo => $titulo,
		receta => $resultado
		);

echo $template -> render($datos);
?>