<?php
include 'lib/funciones.php';

login();

$usuario = $_COOKIE["user"];
//$usuario = $_SESSION["user"];
$user = user(0, $usuario);

$receta = $_GET["receta_id"];
$rec = detalle($receta);

if ($user["id"] != $rec["usuario_id"]){
	header("Location: noreceta.php");
}

$template = $twig -> loadTemplate("recetas/eliminarreceta.html");
$titulo = "Eliminar Receta: ";
$datos = array(
		titulo => $titulo,
		receta => $rec
		);
echo $template -> render($datos);
?>