<?php
// Datos de configuracion de la app. Ej: user, pass y bd

// Datos del administrador;
$root = "root";
$passwd_admin = "root";

// Configuración twig
require_once realpath(dirname(__FILE__) . "/../vendor/twig/twig/lib/Twig/Autoloader.php");
								// __FILE__ da el nombre del archivo
					// Dirname da el nombre del directorio
			// Realpath da la ruta absoluta del archivo si no existe devuelve null
			
/*	Crear un objeto $objeto = new clase;
 * 	Ejecutar un metodo del $objeto -> metodo();
 * 
 *  Los siguientes pasos cargan los metodos de Twig para poder utilizarlos después
 */
 Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__) . "/../templates"));
$twig = new Twig_Environment($loader);


?>