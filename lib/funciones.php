<?php
include ('config.php');

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
// Eliminamos el posible codigo inyectado
function clean($dato){
	return htmlentities(strip_tags(trim($dato)));
									// Quita los espacios en blanco
						// Cambia los caracteres especiales por html. Ej: si aparece < lo cambia por &lt;
			// Elimina las posibles 
}

function listarrecetas($titulo){
	try {
		$consulta = "SELECT apprecetario_receta.*, auth_user.username FROM apprecetario_receta, auth_user WHERE apprecetario_receta.usuario_id=auth_user.id ";
		$order = "ORDER BY creacion DESC";
		
		switch ($titulo) {
			case "Inicio":
				$consulta = $consulta . $order . " LIMIT 20";
				break;
			case "Listado Recetas":
				$consulta = $consulta . $order;
				break;
			case "Listado Entrantes":
				$consulta = $consulta . "AND tipo = 'Entrante' " . $order;
				break;
			case "Listado Primeros":
				$consulta = $consulta . "AND tipo = 'Primero' " . $order;
				break;
			case "Listado Segundos":
				$consulta = $consulta . "AND tipo = 'Segundo' " . $order;
				break;
			case "Listado Postres":
				$consulta = $consulta . "AND tipo = 'Postre' " . $order;
				break;
		}
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> query($consulta);
		$resultado = array();
		foreach ($sentencia as $dato) {
			$resultado[] = array(
							"id" => $dato[0],
							"nombre" => $dato[1],
							"elaboracion" => $dato[2],
							"tipo" => $dato[3],
							"duracion" => $dato[4],
							"creacion" => $dato[5],
							"edicion" => $dato[6],
							"imagen" => $dato[7],
							"usuario_id" => $dato[8],
							"ingredientes" => $dato[9],
							"usuario" => $dato[10]
							);
		}
		$conn = null;
		return $resultado;
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}		
}

function listarusuario($usuario_id){
	try {
		$consulta = "SELECT apprecetario_receta.*, auth_user.username FROM apprecetario_receta, auth_user WHERE apprecetario_receta.usuario_id=auth_user.id AND apprecetario_receta.usuario_id = :usuario_id ORDER BY creacion DESC";
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> prepare($consulta);
		$id = (integer)$usuario_id;
		$sentencia -> bindParam(':usuario_id', $id);
		$sentencia -> execute();
		$resultado = array();
		foreach ($sentencia as $dato) {
			$resultado[] = array(
							"id" => $dato[0],
							"nombre" => $dato[1],
							"elaboracion" => $dato[2],
							"tipo" => $dato[3],
							"duracion" => $dato[4],
							"creacion" => $dato[5],
							"edicion" => $dato[6],
							"imagen" => $dato[7],
							"usuario_id" => $dato[8],
							"ingredientes" => $dato[9],
							"usuario" => $dato[10]
							);
		}
		$conn = null;
		return $resultado;
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}		
}

function buscarrecetas($receta, $ingrediente, $usuario, $tipos){
	try {
		$consulta = "SELECT apprecetario_receta.*, auth_user.username FROM apprecetario_receta, auth_user WHERE apprecetario_receta.usuario_id=auth_user.id ";
		$order = "ORDER BY apprecetario_receta.creacion DESC";
		$tiposstr = "";
		
		if ($receta != 'receta') {
			$consulta = $consulta . "AND apprecetario_receta.nombre LIKE :receta ";
		}
		if ($ingrediente != 'ingrediente') {
			$consulta = $consulta . "AND apprecetario_receta.ingredientes LIKE :ingredientes ";
		}
		if ($usuario != 'usuario') {
			$consulta = $consulta . "AND auth_user.username LIKE :usuario ";
		}
		if (!empty($tipos)) {
			$consulta = $consulta . 'AND apprecetario_receta.tipo IN (:tipos) ';
			foreach ($tipos as $tipo) {
				$tiposstr = $tiposstr . $tipo . ', ';
			}
			$tiposstr = substr($tiposstr, 0, -2);
		}
		$consulta = $consulta . $order;
		
		$receta = "%" . $receta . "%";
		$ingrediente = "%" . $ingrediente . "%";
		$usuario = "%" . $usuario . "%";
		
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> prepare($consulta);
		
		if ($receta != 'receta') {
			$sentencia -> bindParam(':receta', $receta, PDO::PARAM_STR);
		}
		if ($ingrediente != 'ingrediente') {
			$sentencia -> bindParam(':ingredientes', $ingrediente, PDO::PARAM_STR);
		}
		if ($usuario != 'usuario') {
			$sentencia -> bindParam(':usuario', $usuario, PDO::PARAM_STR);
		}
		if (!empty($tipos)) {
			$sentencia -> bindParam(':tipos', $tiposstr, PDO::PARAM_STR);
		}
		
		$sentencia -> execute();
		$resultado = array();	
		foreach ($sentencia as $dato) {
			$resultado[] = array(
							"id" => $dato[0],
							"nombre" => $dato[1],
							"elaboracion" => $dato[2],
							"tipo" => $dato[3],
							"duracion" => $dato[4],
							"creacion" => $dato[5],
							"edicion" => $dato[6],
							"imagen" => $dato[7],
							"usuario_id" => $dato[8],
							"ingredientes" => $dato[9],
							"usuario" => $dato[10]
							);
		}
		$conn = null;
		return $resultado;
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}		
}

function detalle($receta_id){
	try {
		$consulta = "SELECT apprecetario_receta.*, auth_user.username FROM apprecetario_receta, auth_user WHERE apprecetario_receta.usuario_id=auth_user.id AND apprecetario_receta.id = :receta_id";
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> prepare($consulta);
		$id = (integer)$receta_id;
		$sentencia -> bindParam(':receta_id', $id);
		$sentencia -> execute();
		$resultado = array();	
		foreach ($sentencia as $dato) {
			$resultado = array(
							"id" => $dato[0],
							"nombre" => $dato[1],
							"elaboracion" => $dato[2],
							"tipo" => $dato[3],
							"duracion" => $dato[4],
							"creacion" => $dato[5],
							"edicion" => $dato[6],
							"imagen" => $dato[7],
							"usuario_id" => $dato[8],
							"ingredientes" => $dato[9],
							"usuario" => $dato[10]
							);
		}
		$conn = null;
		return $resultado;
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}	
}

function nuevareceta($receta){
	$consulta = "INSERT INTO apprecetario_receta VALUES (:id, :nombre, :elaboracion, :tipo, :duracion, :creacion, :edicion, :imagen, :usuario_id, :ingredientes)";
	try {
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> prepare($consulta);
        
        $hoy = date("d-m-Y");
		$receta["creacion"] = $hoy;
        $receta["edicion"] = $hoy;
        $sentencia -> bindParam(':nombre',  $receta["nombre"]);
        $sentencia -> bindParam(':tipo',  $receta["tipo"]);
        $sentencia -> bindParam(':duracion',  $receta["duracion"]);
        $sentencia -> bindParam(':ingredientes',  $receta["ingredientes"]);
        $sentencia -> bindParam(':elaboracion',  $receta["elaboracion"]);
        $sentencia -> bindParam(':usuario_id',  $receta["usuario_id"]);
        $sentencia -> bindParam(':id',  $receta["id"]);
        $sentencia -> bindParam(':creacion',  $receta["creacion"]);
        $sentencia -> bindParam(':edicion',  $receta["edicion"]);
        $sentencia -> bindParam(':imagen',  $receta["imagen"]);
        $sentencia -> execute();
                
		$conn = null;
		
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}
}

function editarreceta($receta){
	$consulta = "UPDATE apprecetario_receta SET nombre=:nombre, elaboracion=:elaboracion, tipo=:tipo, duracion=:duracion, edicion=:edicion, ingredientes=:ingredientes";
	if ($receta["imagen"] != ""){
		$consulta = $consulta . ", imagen=:imagen";
	}
	$consulta = $consulta . " WHERE id=:id";
	try {
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> prepare($consulta);
		
        $hoy = date("d-m-Y");
        $receta["edicion"] = $hoy;
        $sentencia -> bindParam(':nombre',  $receta["nombre"]);
        $sentencia -> bindParam(':tipo',  $receta["tipo"]);
        $sentencia -> bindParam(':duracion',  $receta["duracion"]);
        $sentencia -> bindParam(':ingredientes',  $receta["ingredientes"]);
        $sentencia -> bindParam(':elaboracion',  $receta["elaboracion"]);
        $sentencia -> bindParam(':id',  $receta["id"]);
        $sentencia -> bindParam(':edicion',  $receta["edicion"]);
		if ($receta["imagen"] != ""){
        	$sentencia -> bindParam(':imagen',  $receta["imagen"]);
        }
        $sentencia -> execute();
                
		$conn = null;
		
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}
}

function eliminarreceta($receta){
	$consulta = "DELETE FROM apprecetario_receta WHERE id = :id";
	try {
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> prepare($consulta);
		$receta = (int)$receta;		
        $sentencia -> bindParam(':id',  $receta);
        $sentencia -> execute();
                
		$conn = null;
		
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}
}

function receta_id(){	
	try {
		$consulta = "SELECT * FROM apprecetario_receta ORDER BY id DESC LIMIT 1";
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> query($consulta);
		$resultado = 0;
		foreach ($sentencia as $dato) {
			$resultado = $dato["id"] + 1;
		}
		$conn = null;
		return $resultado;
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}
}

function user($op, $usuario){
	try {
		if ($op == 0){
			$consulta = "SELECT * FROM auth_user WHERE username = :username";
		}else{
			$consulta = "SELECT * FROM auth_user WHERE id = :usuario_id";
		}
		$conn = new PDO('sqlite:recetario.sqlite3', null, null, array(PDO::ATTR_PERSISTENT => true));
		$sentencia = $conn -> prepare($consulta);
		if ($op == 0){			
			$sentencia -> bindParam(':username', $usuario);
		}else{
			$id = (integer)$usuario;
			$sentencia -> bindParam(':usuario_id', $id);
		}
		$sentencia -> execute();
		$resultado = "";	
		foreach ($sentencia as $dato) {
			$resultado = array (
						"id" => $dato[0],
						"username" => $dato[4],
						"first_name" => $dato[5],
						"last_name" => $dato[6],
						"email" => $dato[7]
						);
		}
		$conn = null;
		return $resultado;
	} catch(PDOException $e ){
			echo $e -> getMessage();
	}
}

function login(){
	/*session_start();
	if (!isset($_SESSION["user"], $_SESSION["passwd"])){		
		header("Location: login.php");
	}*/
	if (!isset($_COOKIE["user"], $_COOKIE["passwd"])){		
		header("Location: login.php");
	}
}

function logout(){
	//session_destroy();
	setcookie("user", "", time()-3600);
	setcookie("passwd", "", time()-3600);
	header("Location: index.php");
}

?>