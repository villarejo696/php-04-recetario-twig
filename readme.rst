RECETARIO
=========

Funcionalidad:
--------------
	- Aplicación creada en PHP con gestión de templates con Twig.
	- Gestiona una base de datos sqlite.
	- Se utilizan sesiones de usuarios mediante COOKIES.
	- Se gestiona la creación, edición y eliminación de recetas.
	- Cada usuario solo podrá editar y/o eliminar sus recetas.
	- De cada receta se gestiona: el nombre, el tipo (entrate, primero,	segundo o postre), las fechas de creación y modificación, el tiempo de elaboración, la elaboración y una imagen.
	- La aplicación se asemeja a la de este repositorio, https://villarejo696@bitbucket.org/villarejo696/django_02_recetario salvo que sólo existe el usuario pepe con password pepe y no existe gestión de usuarios.
			
Instalación:
------------
	- Descarga o clona el proyecto de Bitbukect:	
		
		::
	
			git clone https://villarejo696@bitbucket.org/villarejo696/php-04-recetario-twig.git
			
	- Instalamos composer en nuestro proyecto:
	
		::
		
			curl -s http://getcomposer.org/installer | php
			
	- Instalamos los requisitos del archivo composer.json:
		
		::
			
			php composer.phar install
		
Uso:
----
	- **Inicio:** Se visualizan hasta 20 recetas ordenadas por fecha de creación de más reciente a más antigua.
	- **Recetas:** Se visualizan de cada receta: el usuario que la creó, cuando se creó o modificó, el nombre, el tipo, los ingredientes, el tiempo de elaboración, la elaboración y una imagen de la misma.
	- **Listados:** Se podrán ver los listados completos de todas las recetas, todas las recetas de un usuario en conreto, de todos los entrantes, de todos los primeros, de todos los segundos o de todos los postres.
	- **Busquedas:** Se podrán buscar recetas por su nombre, por el tipo, por alguno de sus ingredientes o por el nombre del usuario.
	- **Gestión Recetas:** El usuario pepe (ya que es el único) podrá: tras acceder al perfil y loguearse, crear sus recetas, estas sólo podrán ser editadas y eliminadas por él mismo, si hace click en Salir las cookies de sesiones y el usuario deberá loguearse otra vez.