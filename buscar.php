<?php
	include 'lib/funciones.php';
	
	$receta = "";
	$ingrediente = "";
	$usuario = "";
	$platos =  array('Entrante', 'Primero', 'Segundo', 'Postre');
	$tipos = array();
	
	if (isset($_POST['receta'])) {
		$receta = clean($_POST['receta']);
	}
	if (isset($_POST['ingrediente'])) {
		$ingrediente = clean($_POST['ingrediente']);
	}
	if (isset($_POST['usuario'])) {
		$usuario = clean($_POST['usuario']);
	}
	if (!empty($_POST['tipos'])) {
		foreach ($_POST['tipos'] as $tipo){
			$tipos[] = $tipo;
		}
	}
	
	$template = $twig -> loadTemplate("buscar.html");
	$titulo = "Buscador de recetas";
	$resultado = buscarrecetas($receta, $ingrediente, $usuario, $tipos);
	$datos = array(
			titulo => $titulo,
			receta => $receta,
			ingrediente => $ingrediente,
			usuario => $usuario,
			platos => $platos,
			tipos => $tipos,
			recetas => $resultado
			);				
	echo $template -> render($datos);
?>