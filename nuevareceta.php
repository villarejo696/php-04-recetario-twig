<?php
include 'lib/funciones.php';

login();

$receta = array();	
	
if ($_POST["nombre"] != "" && $_POST["tipo"] != "" && $_POST["duracion"] != "" &&  !is_int($_POST["duracion"]) && $_POST["ingredientes"] != "" && $_POST["elaboracion"] != ""){
	$receta["nombre"] = $_POST["nombre"];
	$receta["tipo"] = $_POST["tipo"];
	$receta["duracion"] = $_POST["duracion"];
	$receta["ingredientes"] = $_POST["ingredientes"];
	$receta["elaboracion"] = $_POST["elaboracion"];
	$usuario = $_COOKIE["user"];
	//$usuario = $_SESSION["user"];
	$user = user(0, $usuario);
	$receta["usuario_id"] = (int)$user["id"];
        $receta["id"] = receta_id();
	if ($_FILES["imagen"]["name"] != ""){
                // Ruta relativa carpeta donde quieres guardar la imagen
		$target_path = "images_rec/";
                // Ruta relativa de la imagen que aparecerá después en la BD como en Django
                // En este caso será un string así images_rec/receta1-[nombredelarchivo]
                // Importante poner al final $_FILES['imagen']['name']
                // Puesto que ya lleva incorporado el formato de la imagen Ej: .jpg
		$target_path = $target_path . "receta" . $receta["id"] . "-" . $_FILES['imagen']['name'];
                // Copiar la imagen a la ruta y con el nombre que hemos indicado
		copy($_FILES['imagen']['tmp_name'], $target_path);
                // Guardo la ruta de la imagen para después insertarla en la BD
		$receta["imagen"] = $target_path;
	}else{
		$receta["imagen"] = "images_rec/cocinillas.jpg";
	}
	nuevareceta($receta);
	header("Location: perfil.php");
}

$template = $twig -> loadTemplate("recetas/nuevareceta.html");
$titulo = "Nueva Receta";
$datos = array(
		titulo => $titulo
		);
		
echo $template -> render($datos);
?>